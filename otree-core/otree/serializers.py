#!/usr/bin/env python
# encoding: utf-8

from otree.export import get_field_names_for_live_update
from rest_framework import serializers
from otree.models import Participant, Session


class SessionSerializer(serializers.ModelSerializer):
    class Meta:
    	model = Session
    	fields = get_field_names_for_live_update(Session)

class ParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Participant
        fields = get_field_names_for_live_update(Participant)


class ParticipantLinkSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=32)
    link = serializers.CharField(max_length=256)

class ParticipantPayoffSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=32)
    payoff = serializers.FloatField()    
    money_to_pay = serializers.FloatField()    
