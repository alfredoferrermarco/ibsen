#!/usr/bin/env python
# encoding: utf-8

from django.views.generic import View
from django.http import JsonResponse, HttpResponse
from django.http import Http404

from rest_framework import generics, permissions
from django.core import serializers

from otree.models import Session, Participant
from otree.serializers import SessionSerializer, ParticipantSerializer, ParticipantLinkSerializer, ParticipantPayoffSerializer

import datetime
import json
import logging
logger = logging.getLogger(__name__)


class Ping(View):

    def get(self, request):
        response = JsonResponse({"ping": True})
        response["Access-Control-Allow-Origin"] = "*"
        return response


class SessionsList(generics.ListCreateAPIView):
    serializer_class = SessionSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):
        try:
            sessions = Session.objects
        except Session.DoesNotExist:
            raise Http404('No Sessions available')
        return sessions.all()



def SessionParticipantsPageList(request):
    
    session_code = request.GET.get('session_code')
    page = 1
    if 'page' in request.GET.keys():
        page = int(request.GET.get('page'))
    try:
        session = Session.objects.get(code=session_code)
        participants_db = Participant.objects.filter(session=session).all()[(page-1)*100:page*100]
        participants = []
        for p in participants_db:
            participants.append({'_id_in_session':p._id_in_session(), 'code':p.code , 'label':p.label, '_current_page':p._current_page(), '_current_app_name':p._current_app_name, '_round_number': p._round_number, '_current_page_name':p._current_page_name, 'status':p.status(), '_last_page_timestamp': p._last_page_timestamp})


    except Session.DoesNotExist:
        raise Http404('This session no longer exists.')

    num_pages = int(session.num_participants / 100.01 ) + 1
    result = {"participants":participants, "number":page, "num_pages": num_pages }
    return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json")        
        

class SessionParticipantsList(generics.ListCreateAPIView):
    serializer_class = ParticipantSerializer
    permission_classes = [permissions.AllowAny]

    def get_queryset(self):

        session_code = self.kwargs['session_code']
        number_of_links = None
        if 'links_number' in self.kwargs.keys():
            number_of_links = int(self.kwargs['links_number'])
        try:
            session = Session.objects.get(code=session_code)
        except Session.DoesNotExist:
            raise Http404('This session no longer exists.')
        return session.get_participants()[:number_of_links] if number_of_links else session.get_participants()

class SessionParticipantsLinks(generics.ListCreateAPIView):
    serializer_class = ParticipantLinkSerializer
    permission_classes = [permissions.AllowAny]
    def get_queryset(self):
        session_code = self.kwargs['session_code']
        number_of_links = int(self.kwargs['links_number'])
        participant_urls = []  
        for participant in Session.objects.get(code=session_code).get_participants()[:number_of_links]:
            link = self.request.build_absolute_uri(participant._start_url())
            participant_urls.append({'code': participant.code, 'link': link})
        return participant_urls

class SessionParticipantsPayoffs(generics.ListCreateAPIView):
    serializer_class = ParticipantPayoffSerializer
    permission_classes = [permissions.AllowAny]
    def get_queryset(self):
        session_code = self.kwargs['session_code']
        number_of_links = int(self.kwargs['links_number'])
        participant_urls = []  
        for participant in Session.objects.get(code=session_code).get_participants()[:number_of_links]:
            link = self.request.build_absolute_uri(participant._start_url())
            participant_urls.append({'code': participant.code, 'payoff': participant.payoff, 'money_to_pay': participant.money_to_pay})
        return participant_urls

