#!/usr/bin/env python
# -*- coding: utf-8 -*-

from otree_save_the_change.mixins import SaveTheChange

from otree.db import models
from otree.common_internal import get_models_module
from otree.models.fieldchecks import ensure_field


class BaseLink(SaveTheChange, models.Model):
    """Base class for all Links.
    """

    class Meta:
        abstract = True
        index_together = ['session']
        ordering = ['pk']

    round_number = models.PositiveIntegerField(db_index=True)

    def __unicode__(self):
        return str(self.pk)

    id_in_player = models.PositiveIntegerField(
        null=True,
        db_index=True,
        doc=("Index starting from 1. In multiplayer games, "
             "indicates whether this is link 1, link 2, etc.")
    )

    def in_round(self, round_number):
        '''You should not use this method if
        you are rearranging links between rounds.'''

        return type(self).objects.filter(
            session=self.session,
            round_number=round_number,
        )

    def in_rounds(self, first, last):
        '''You should not use this method if
        you are rearranging links between rounds.'''

        qs = type(self).objects.filter(
            session=self.session,
            round_number__gte=first,
            round_number__lte=last,
        ).order_by('round_number')

        ret = list(qs)
        assert len(ret) == last-first+1
        return ret

    def in_previous_rounds(self):
        return self.in_rounds(1, self.round_number-1)

    def in_all_rounds(self):
        return self.in_previous_rounds() + [self]

    def _PlayerClass(self):
        return self._meta.get_field('player').rel.to

    def _PlayerClass(self):
        return self._meta.get_field('neighbor').rel.to

    @property
    def _Constants(self):
        return get_models_module(self._meta.app_config.name).Constants

    @classmethod
    def _ensure_required_fields(cls):
        """
        Every ``Link`` model requires a foreign key to the ``Player`` and
        ``Group`` model of the same app.
        """
        player_model = '{app_label}.Player'.format(
            app_label=cls._meta.app_label)
        player_field = models.ForeignKey(player_model)
        ensure_field(cls, 'player', player_field)

        player_model = '{app_label}.Player'.format(
            app_label=cls._meta.app_label)
        player_field = models.ForeignKey(player_model, related_name="neighbor")
        ensure_field(cls, 'neighbor', player_field)

        subsession_model = '{app_label}.Subsession'.format(
            app_label=cls._meta.app_label)
        subsession_field = models.ForeignKey(subsession_model)
        ensure_field(cls, 'subsession', subsession_field)

        subsession_model = '{app_label}.Group'.format(
            app_label=cls._meta.app_label)
        subsession_field = models.ForeignKey(subsession_model)
        ensure_field(cls, 'group', subsession_field)
