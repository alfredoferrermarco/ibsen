# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import otree.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '0015_auto_20161111_1055'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='mail_content',
            field=otree.db.models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='session',
            name='_pre_create_id',
            field=otree.db.models.CharField(max_length=300, null=True, db_index=True),
        ),
    ]
