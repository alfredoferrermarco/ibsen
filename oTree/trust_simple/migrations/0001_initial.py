# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal
from otree.common import Currency
import otree_save_the_change.mixins
import otree.db.models


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('sent_amount', otree.db.models.CurrencyField(null=True, max_digits=12, choices=[(Currency(0), Currency(0)), (Currency(1), Currency(1)), (Currency(2), Currency(2)), (Currency(3), Currency(3)), (Currency(4), Currency(4)), (Currency(5), Currency(5)), (Currency(6), Currency(6)), (Currency(7), Currency(7)), (Currency(8), Currency(8)), (Currency(9), Currency(9)), (Currency(10), Currency(10))])),
                ('sent_back_amount', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('session', otree.db.models.ForeignKey(related_name='trust_simple_group', to='otree.Session')),
            ],
            options={
                'db_table': 'trust_simple_group',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_index_in_game_pages', otree.db.models.PositiveIntegerField(default=0, null=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_group', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('group', otree.db.models.ForeignKey(to='trust_simple.Group', null=True)),
                ('participant', otree.db.models.ForeignKey(related_name='trust_simple_player', to='otree.Participant')),
                ('session', otree.db.models.ForeignKey(related_name='trust_simple_player', to='otree.Session')),
            ],
            options={
                'db_table': 'trust_simple_player',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Subsession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('session', otree.db.models.ForeignKey(related_name='trust_simple_subsession', to='otree.Session', null=True)),
            ],
            options={
                'db_table': 'trust_simple_subsession',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.AddField(
            model_name='player',
            name='subsession',
            field=otree.db.models.ForeignKey(to='trust_simple.Subsession'),
        ),
        migrations.AddField(
            model_name='group',
            name='subsession',
            field=otree.db.models.ForeignKey(to='trust_simple.Subsession'),
        ),
    ]
