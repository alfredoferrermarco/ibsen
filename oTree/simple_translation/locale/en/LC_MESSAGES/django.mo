��          T      �       �      �      �      �      �      �      	  @    t   X  �   �    _     o     |  "   �                                        instructions_p1 instructions_p2 instructions_p3 instructions_title welcome_text welcome_title Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-03-31 14:27+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Create your app following the usual way. 'otree startapp app_name' . Create a folder 'locale' inside app_name folder Check the source code of this app to see how to define the strings to translate in html and how to detect the client language and write your app. Run 'django-admin makemessages --locale=en' if you want the english language supported. This command will create an 'en' folder inside the locale folder. For each msgid, fill the msgstr field with the corresponding translation. Finally run 'django-admin compilemessages'. Instructions Welcome Your first IBSEN multilanguage app 