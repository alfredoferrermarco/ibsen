# -*- coding: utf-8 -*-
# <standard imports>
from __future__ import division

import random

import otree.models
from otree.db import models
from otree import widgets
from otree.common import Currency as c, currency_range, safe_json
from otree.constants import BaseConstants
from otree.models import BaseSubsession, BaseGroup, BasePlayer
# </standard imports>

author = 'M. Pereda'

doc = """
Public Goods Game with two treatments: individual performance and group performance
"""


class Constants(BaseConstants):
    name_in_url = 'PGG'
    players_per_group = 6
    num_rounds = 10
    endowment = c(100)
    multiplication_factor=2

    # define more constants here


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    # <built-in>
    subsession = models.ForeignKey(Subsession)
    # </built-in>
    total_contribution = models.CurrencyField()
    individual_share = models.CurrencyField()
    hierarchy = models.IntegerField()
    
    def set_payoffs(self):
        self.total_contribution=sum([p.contribution for p in self.get_players()])
        self.individual_share = self.total_contribution * Constants.multiplication_factor / Constants.players_per_group
        for p in self.get_players():
            p.payoff = Constants.endowment - p.contribution + self.individual_share


class Player(BasePlayer):
    # <built-in>
    subsession = models.ForeignKey(Subsession)
    group = models.ForeignKey(Group, null=True)
    # </built-in>
    contribution = models.CurrencyField(min=0, max=Constants.endowment)

    def role(self):
        # you can make this depend of self.id_in_group
        return ''
