# -*- coding: utf-8 -*-
# <standard imports>
from __future__ import division

import random
import itertools #to use cycle

import otree.models
from otree.db import models
from otree import widgets
from otree.common import Currency as c, currency_range, safe_json
from otree.constants import BaseConstants
from otree.models import BaseSubsession, BaseGroup, BasePlayer, BaseLink
# </standard imports>

author = 'M. Pereda'

doc = """
Public Goods Game with two treatments: individual performance and group performance
"""


class Constants(BaseConstants):
    name_in_url = 'PGG'

    players_per_group = 4
    num_rounds = 15
    endowment = c(10)
    multiplication_factor=2

    #APD constants for General Instructions Page
    APD_num_phases = 5
    lower_bound_APD_num_rounds_per_phase = 10 - 5
    players_per_hierarchy_level = 6
    

    #Multipliers per hierarchy level
    A_multiplier = 5
    B_multiplier = 4
    C_multiplier = 3
    D_multiplier = 2

    num_phases = 5

    # APD Payoffs
    Y_X_amount = 4 #Not in points if i don't use c()
    X_amount = 3
    X_Y_amount = 1
    Y_amount = 2

    #Correct answers to questionaire
    questionsI1_correct = c(10)
    questionsI2_correct = c(20)
    questionsI3_correct = c(15)
    questionsI4_correct = c(16)
    questionsI5_correct = 'B'
    questionsI6_correct = 'C'
    questionsI5_correct2 = 'b'
    questionsI6_correct2 = 'c'

    questionsII7_correct = 4
    questionsII8_correct = 8
    questionsII9_correct = 6
    questionsII10_correct = 15


class Subsession(BaseSubsession):
    def before_session_starts(self):

        # Randomizing groups
        if self.round_number == 1:
            players = self.get_players()
            random.shuffle(players)
            group_matrix = []
            # chunk into groups of Constants.players_per_group
            for i in range(0, len(players), Constants.players_per_group):
                group_matrix.append(players[i:i+Constants.players_per_group])
            self.set_groups(group_matrix)

        else:
            self.group_like_round(1)


        # select to play a treatment
        if 'treatment' in self.session.config:
            for g in self.get_groups():
                g.treatment = self.session.config['treatment']

        else:
            treatments = itertools.cycle(['group', 'individual'])
            for g in self.get_groups():
                # randomize two treatments
                g.treatment = treatments.next()
       
            
        if self.round_number == 1:
            for p in self.get_players():
                p.participant.vars['cumulative_payoff'] = 0
                p.participant.vars['groupal_payoff'] = 0
                p.participant.vars['combined_payoff'] = 0
                p.participant.vars['prev_payoff'] = 0
                p.payoff = 0


               
    def set_hierarchy(self):
        
        if (('treatment' in self.session.config) and (self.session.config['treatment']=='individual')) or not('treatment' in self.session.config):
            groups_individual = [item for item in self.get_groups() if item.treatment=='individual']
            players_individual = []
            for g in groups_individual:
                players_individual = players_individual + g.get_players()
                # players_individual.append(g.get_players())
            
            # reverse = True for descending order sorting
            sorted_players_i = sorted(players_individual,key=lambda player: player.participant.vars['cumulative_payoff'], reverse = True)
            pph = Constants.players_per_hierarchy_level
            for i in range(0, len(sorted_players_i), pph):
                for j in sorted_players_i[i:i+pph]:
                    j.participant.vars['hierarchy'] =  int(len(players_individual)/pph - i/pph)
                    if (i/pph == 0): #higher level
                        j.participant.vars['profile']='A'
                        j.participant.vars['multiplier']=Constants.A_multiplier
                    elif (i/pph == 1): #second level
                        j.participant.vars['profile']='B'
                        j.participant.vars['multiplier']=Constants.B_multiplier
                    elif (i/pph == 2): #third level
                        j.participant.vars['profile']='C'
                        j.participant.vars['multiplier']=Constants.C_multiplier
                    else:
                        j.participant.vars['profile']='D' #the rest
                        j.participant.vars['multiplier']=Constants.D_multiplier

        # ('treatment' in self.session.config==False):
        if (('treatment' in self.session.config) and (self.session.config['treatment']=='group'))  or not('treatment' in self.session.config):
            #Esto para coger a los players del tratamiento de grupo en caso de que se jueguen dos tratamientos a la vez
            groups_group = [item for item in self.get_groups() if item.treatment=='group']
            players_group = []
            for g in groups_group:
                players_group = players_group + g.get_players()
                # players_individual.append(g.get_players())
            for p in players_group:
                # Combined meassure to rank participants that priorices group rank and also ranks participants within groups
                p.participant.vars['combined_payoff'] = p.group.artificial_payoff + p.participant.vars['cumulative_payoff']

            sorted_players_g = sorted(players_group,key=lambda player: player.participant.vars['combined_payoff'], reverse = True)
            pph = Constants.players_per_hierarchy_level
            for i in range(0, len(sorted_players_g), pph):
                for j in sorted_players_g[i:i+pph]:
                    j.participant.vars['hierarchy'] =  int(len(players_group)/pph - i/pph)
                    
                    if (i/pph == 0): #higher level
                        j.participant.vars['profile']='A'
                        j.participant.vars['multiplier']=Constants.A_multiplier
                    elif (i/pph == 1): #second level
                        j.participant.vars['profile']='B'
                        j.participant.vars['multiplier']=Constants.B_multiplier
                    elif (i/pph == 2): #third level
                        j.participant.vars['profile']='C'
                        j.participant.vars['multiplier']=Constants.C_multiplier
                    else:
                        j.participant.vars['profile']='D' #the rest
                        j.participant.vars['multiplier']=Constants.D_multiplier

            for i in range(0, len(sorted_players_g), Constants.players_per_group):
                for j in sorted_players_g[i:i+Constants.players_per_group]:
                    j.participant.vars['group_rank'] =  int(i/Constants.players_per_group + 1)

            
            ind = 1
            for i in (sorted_players_g):
                i.participant.vars['ind_rank_in_group']= int(ind)  - int((i.participant.vars['group_rank'] - 1) * Constants.players_per_group )
                ind = ind + 1

        #Update player variables
        for p in self.get_players():
            p.profile = p.participant.vars['profile']
            p.multiplier = p.participant.vars['multiplier']

            if p.group.treatment=='group':
                p.ind_rank_in_group = p.participant.vars['ind_rank_in_group']
                p.group_rank = p.participant.vars['group_rank']

                    


class Group(BaseGroup):
    # <built-in>
    subsession = models.ForeignKey(Subsession)
    # </built-in>
    total_contribution = models.CurrencyField()
    individual_share = models.CurrencyField()
    treatment = models.CharField()
    num_timeouts = models.IntegerField()
    groupal_payoff = models.CurrencyField()
    artificial_payoff = models.CurrencyField()
    
    def set_payoffs(self):
        self.total_contribution = sum([p.contribution for p in self.get_players()])
        self.individual_share = self.total_contribution * Constants.multiplication_factor / Constants.players_per_group
        
        for p in self.get_players():
            p.payoff = Constants.endowment - p.contribution + self.individual_share
            p.participant.vars['prev_payoff']=p.payoff
        
            

class Link(BaseLink):
    pass

    
class Player(BasePlayer):
    subsession = models.ForeignKey(Subsession)
    group = models.ForeignKey(Group, null=True)
    
    choices = range(0,Constants.endowment + 1,2)
    contribution = models.CurrencyField(initial=None, widget=widgets.RadioSelectHorizontal(),verbose_name='¿Cuántos puntos quieres contribuir al fondo común?', choices=choices)

    #Phase I questions
    questionsI1 = models.CurrencyField()
    questionsI2 = models.CurrencyField()
    questionsI3 = models.CurrencyField()
    questionsI4 = models.CurrencyField()
    questionsI5 = models.CharField()
    questionsI6 = models.CharField()


    def questionsI1_correct(self):
        return self.questionsI1== Constants.questionsI1_correct

    def questionsI2_correct(self):
        return self.questionsI2== Constants.questionsI2_correct

    def questionsI3_correct(self):
        return self.questionsI3== Constants.questionsI3_correct

    def questionsI4_correct(self):
        return self.questionsI4== Constants.questionsI4_correct

    def questionsI5_correct(self):
        #not case sensitive
        return ((self.questionsI5== Constants.questionsI5_correct) or (self.questionsI5== Constants.questionsI5_correct2))

    def questionsI6_correct(self):
        return ((self.questionsI6== Constants.questionsI6_correct) or (self.questionsI6== Constants.questionsI6_correct2))


    #Phase II questions
    questionsII7 = models.IntegerField()
    questionsII8 = models.IntegerField()
    questionsII9 = models.IntegerField()
    questionsII10 = models.IntegerField()


    def questionsII7_correct(self):
        return self.questionsII7== Constants.questionsII7_correct
    def questionsII8_correct(self):
        return self.questionsII8== Constants.questionsII8_correct
    def questionsII9_correct(self):
        return self.questionsII9== Constants.questionsII9_correct
    def questionsII10_correct(self):
        return self.questionsII10== Constants.questionsII10_correct

    
    # I define player variables to store the values to be exported since participant variables are not yet exported in this otree version
    cumulative_payoff = models.CurrencyField()   
    profile = models.CharField()
    multiplier = models.IntegerField()
    ind_rank_in_group = models.IntegerField()
    group_rank = models.IntegerField()
    automatic_decision = models.BooleanField()

