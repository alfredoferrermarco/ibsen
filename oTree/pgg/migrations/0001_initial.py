# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import otree.db.models
import otree_save_the_change.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('total_contribution', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('individual_share', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('treatment', otree.db.models.CharField(max_length=500, null=True)),
                ('num_timeouts', otree.db.models.IntegerField(null=True)),
                ('groupal_payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('artificial_payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('session', otree.db.models.ForeignKey(related_name='pgg_group', to='otree.Session')),
            ],
            options={
                'db_table': 'pgg_group',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_index_in_game_pages', otree.db.models.PositiveIntegerField(default=0, null=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_group', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('contribution', otree.db.models.CurrencyField(null=True, verbose_name=b'\xc2\xbfCu\xc3\xa1ntos puntos quieres contribuir al fondo com\xc3\xban?', max_digits=12, choices=[(0, 0), (2, 2), (4, 4), (6, 6), (8, 8), (10, 10)])),
                ('questionsI1', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('questionsI2', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('questionsI3', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('questionsI4', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('questionsI5', otree.db.models.CharField(max_length=500, null=True)),
                ('questionsI6', otree.db.models.CharField(max_length=500, null=True)),
                ('questionsII7', otree.db.models.IntegerField(null=True)),
                ('questionsII8', otree.db.models.IntegerField(null=True)),
                ('questionsII9', otree.db.models.IntegerField(null=True)),
                ('questionsII10', otree.db.models.IntegerField(null=True)),
                ('cumulative_payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('profile', otree.db.models.CharField(max_length=500, null=True)),
                ('multiplier', otree.db.models.IntegerField(null=True)),
                ('ind_rank_in_group', otree.db.models.IntegerField(null=True)),
                ('group_rank', otree.db.models.IntegerField(null=True)),
                ('automatic_decision', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('group', otree.db.models.ForeignKey(to='pgg.Group', null=True)),
                ('participant', otree.db.models.ForeignKey(related_name='pgg_player', to='otree.Participant')),
                ('session', otree.db.models.ForeignKey(related_name='pgg_player', to='otree.Session')),
            ],
            options={
                'db_table': 'pgg_player',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Subsession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('session', otree.db.models.ForeignKey(related_name='pgg_subsession', to='otree.Session', null=True)),
            ],
            options={
                'db_table': 'pgg_subsession',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.AddField(
            model_name='player',
            name='subsession',
            field=otree.db.models.ForeignKey(to='pgg.Subsession'),
        ),
        migrations.AddField(
            model_name='group',
            name='subsession',
            field=otree.db.models.ForeignKey(to='pgg.Subsession'),
        ),
    ]
