# -*- coding: utf-8 -*-
from __future__ import division

import random

from otree.common import Currency as c, currency_range

from ._builtin import Bot
from .models import Constants
from . import views


class PlayerBot(Bot):

    def play_round(self):

        #Phase page
        if ((self.subsession.round_number - 1)%Constants.num_rounds_per_phase == 0):
            self.submit(views.Phase)

        #Decicion page
        self.submit(views.Decision, {'decision': random.choice(['X', 'Y'])})

        #LastResult page
        if self.subsession.round_number == Constants.num_rounds :
            self.submit(views.LastResult)


    def validate_play(self):
        pass
