# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import otree.db.models
import otree_save_the_change.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('num_timeouts', otree.db.models.IntegerField(null=True)),
                ('session', otree.db.models.ForeignKey(related_name='apd2_group', to='otree.Session')),
            ],
            options={
                'db_table': 'apd2_group',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_index_in_game_pages', otree.db.models.PositiveIntegerField(default=0, null=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_group', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('decision', otree.db.models.CharField(max_length=500, null=True, verbose_name=b'\xc2\xbfCu\xc3\xa1l es tu decisi\xc3\xb3n?')),
                ('cumulative_payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('profile', otree.db.models.CharField(max_length=500, null=True)),
                ('partner_profile', otree.db.models.CharField(max_length=500, null=True)),
                ('partner_ID', otree.db.models.PositiveIntegerField(null=True)),
                ('multiplier', otree.db.models.IntegerField(null=True)),
                ('stage_payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('automatic_decision', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('pairings', otree.db.models.CharField(max_length=500, null=True)),
                ('index', otree.db.models.IntegerField(null=True)),
                ('partner_index', otree.db.models.IntegerField(null=True)),
                ('myID', otree.db.models.PositiveIntegerField(null=True)),
                ('group', otree.db.models.ForeignKey(to='apd2.Group', null=True)),
                ('participant', otree.db.models.ForeignKey(related_name='apd2_player', to='otree.Participant')),
                ('session', otree.db.models.ForeignKey(related_name='apd2_player', to='otree.Session')),
            ],
            options={
                'db_table': 'apd2_player',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Subsession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('session', otree.db.models.ForeignKey(related_name='apd2_subsession', to='otree.Session', null=True)),
            ],
            options={
                'db_table': 'apd2_subsession',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.AddField(
            model_name='player',
            name='subsession',
            field=otree.db.models.ForeignKey(to='apd2.Subsession'),
        ),
        migrations.AddField(
            model_name='group',
            name='subsession',
            field=otree.db.models.ForeignKey(to='apd2.Subsession'),
        ),
    ]
