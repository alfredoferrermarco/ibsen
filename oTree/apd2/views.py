# -*- coding: utf-8 -*-
from __future__ import division
from . import models
from ._builtin import Page, WaitPage
from otree.common import Currency as c, currency_range
from .models import Constants

## To show rate with two decimals and without the currency symbol
from re import sub
from decimal import Decimal

import random 
import json


def vars_for_all_templates(self):

    if 'repetitions' in self.session.config: 
        repet =  self.session.config['repetitions'] 
    else: 
        repet = 1

    return {
            'min_rounds': Constants.num_rounds_per_phase - 5,
            'max_rounds': Constants.num_rounds_per_phase + 5,
            'stage': int(((self.subsession.round_number - 1) / Constants.num_rounds_per_phase) + 1),
            'prev_stage':int(((self.subsession.round_number - 1) / Constants.num_rounds_per_phase)),
            'round_within_stage': (self.subsession.round_number - 1)%Constants.num_rounds_per_phase + 1,
            'rate':  Decimal(sub(r'[^\d.]', '', str(1 / c(1).to_real_world_currency(self.session)))),
            'PhaseII_repetitions': repet,
        }

class Decision(Page):

    #timeout_seconds = 20

    form_model = models.Player
    form_fields = ['decision']

    def decision_choices(self):
        options=['X', 'Y']
        random.shuffle(options)
        return options


    def vars_for_template(self):
        for p in self.subsession.get_players():
                P=p.get_others_in_group()
                partner=P[0]

                # To be exported
                p.partner_profile = p.participant.vars['partner_profile']
                p.profile = p.participant.vars['profile']


    def before_next_page(self):
        if self.timeout_happened:
            self.player.decision = random.choice(['X', 'Y'])
            self.player.automatic_decision = True
                



class ResultsWaitPage(WaitPage):
    body_text = 'Esperando a que tu compañero elija.'

    def after_all_players_arrive(self):

        for g in self.subsession.get_groups():

            #Count the number of timeouts within each group
            num_timeouts = 0
            for p in g.get_players():
                if p.automatic_decision == True:
                    num_timeouts = num_timeouts + 1
            g.num_timeouts = int(num_timeouts)

        for p in self.group.get_players():
            
            #This just to play APD without the previous PGG phase
            if not 'multiplier' in p.participant.vars:
                if p.participant.vars['profile']=='A':
                    p.participant.vars['multiplier']=Constants.A_multiplier
                if p.participant.vars['profile']=='B':
                    p.participant.vars['multiplier']=Constants.B_multiplier
                if p.participant.vars['profile']=='C':
                    p.participant.vars['multiplier']=Constants.C_multiplier
                if p.participant.vars['profile']=='D':
                        p.participant.vars['multiplier']=Constants.D_multiplier

            #To be exported
            p.multiplier = p.participant.vars['multiplier']

            #Updating payoffs
            p.set_payoff()
            p.payoff = p.payoff * c(p.participant.vars['multiplier'])            

            p.participant.vars['prev_payoff']=int(p.payoff) ## I use int casting because i want to name the payoff in ECUs but just in this app
            p.participant.vars['cumulative_payoff2']=int(sum([q.payoff for q in p.in_all_rounds()]))

            prev=int(p.participant.vars['stage_payoff'])
            p.participant.vars['stage_payoff']= int(prev + int(p.payoff))
            p.participant.vars['prev_stage_ECUs']=p.participant.vars['stage_payoff']

            #To be exported
            p.stage_payoff = p.participant.vars['stage_payoff']
            p.cumulative_payoff2 = p.participant.vars['cumulative_payoff2']
    
        for p in self.group.get_players():
            P=p.get_others_in_group()
            partner=P[0]
            p.participant.vars['partner_prev_payoff']=partner.participant.vars['prev_payoff']  


            # # Automated players get nothing in the automated round 
            # if p.automatic_decision == True:
            #     remove = p.payoff
            #     p.payoff = 0
            #     p.participant.vars['prev_payoff']=p.payoff
            #     p.participant.vars['cumulative_payoff2']= p.participant.vars['cumulative_payoff2'] - remove
            #     p.participant.vars['stage_payoff']= p.participant.vars['stage_payoff'] - remove
            #     p.participant.vars['prev_stage_ECUs']=p.participant.vars['stage_payoff']  
                
            #     #Update player variable to be exported
            #     p.stage_payoff = p.participant.vars['stage_payoff']
            #     p.cumulative_payoff2 = p.participant.vars['cumulative_payoff2'] 



class ShuffleWaitPage(WaitPage):
    wait_for_all_groups = True

    def after_all_players_arrive(self):
        
        #AT THE BEGENING OF THE GAME, GROUPS OF PAIRINGS (our columns) ARE RANDOMIZED in order of appearance
        if self.subsession.round_number == 1:
            random.shuffle(self.subsession.pairings)

        

        if ((self.subsession.round_number - 1)%Constants.num_rounds_per_phase == 0): #Each "num_rounds_per_phase" rounds

            current_stage = int(((self.subsession.round_number - 1) / Constants.num_rounds_per_phase))
            
            players = self.subsession.get_players()

            # if the player has 'combined_payoff' atribbute, she has played the group treatment
            if 'combined_payoff' in players[0].participant.vars:
                sorted_players = sorted(players,key=lambda player: player.participant.vars['combined_payoff'], reverse = True)
            else:
                if 'cumulative_payoff_PGG' in players[0].participant.vars:
                    sorted_players = sorted(players,key=lambda player: player.participant.vars['cumulative_payoff_PGG'], reverse = True)
                else:
                    #just for the bots without PGG phase
                    sorted_players = sorted(players,key=lambda player: player.participant.id_in_session, reverse = False)

                
            indexes = self.subsession.pairings[int(current_stage-1)]

            #just for tests
            if len(self.subsession.get_players())==6:
                 group_matrix = [[sorted_players[indexes[0]], sorted_players[indexes[1]]],[sorted_players[indexes[2]],sorted_players[indexes[3]]], [sorted_players[indexes[4]],sorted_players[indexes[5]]] ]
            else:
                group_matrix = [[sorted_players[indexes[0]], sorted_players[indexes[1]]],[sorted_players[indexes[2]],sorted_players[indexes[3]]], [sorted_players[indexes[4]],sorted_players[indexes[5]]], [sorted_players[indexes[6]], sorted_players[indexes[7]]], [sorted_players[indexes[8]], sorted_players[indexes[9]]],[sorted_players[indexes[10]], sorted_players[indexes[11]]], [sorted_players[indexes[12]], sorted_players[indexes[13]]], [sorted_players[indexes[14]], sorted_players[indexes[15]]], [sorted_players[indexes[16]], sorted_players[indexes[17]]], [sorted_players[indexes[18]], sorted_players[indexes[19]]], [sorted_players[indexes[20]], sorted_players[indexes[21]]], [sorted_players[indexes[22]], sorted_players[indexes[23]]] ]
            
            self.subsession.set_groups(group_matrix)


            for p in sorted_players:
                p.pairings = json.dumps(indexes)
                

            for i in range(0, len(sorted_players)):
                sorted_players[i].index = i

            for p in sorted_players:
                P=p.get_others_in_group()
                partner=P[0]
                p.partner_index = partner.index


        else:
            self.subsession.group_like_round( int(self.subsession.round_number - 1))



class Phase(Page):

    ## Update partner information
    def vars_for_template(self):

        profiles_bots = ['A','A','A','A','A','A','B','B','B','B','B','B','C','C','C','C','C','C','D','D','D','D','D','D']
        ind = 0

        for p in self.subsession.get_players():
                
            if not 'profile' in p.participant.vars:
                p.participant.vars['profile']= profiles_bots[ind]
            else:
                # To be exported
                p.profile = p.participant.vars['profile']
            ind = ind + 1

        for p in self.subsession.get_players():
                P=p.get_others_in_group()
                partner=P[0]

                if 'profile' in partner.participant.vars:
                    p.participant.vars['partner_profile']=partner.participant.vars['profile']
                    # To be exported
                    p.partner_profile = p.participant.vars['partner_profile']
                    p.partner_ID = partner.participant.id_in_session
                    
                else:
                    p.participant.vars['partner_profile']='not'
                    


                p.participant.vars['stage_payoff']=0 #initialization


    def is_displayed(self):
        return ((self.subsession.round_number - 1)%Constants.num_rounds_per_phase == 0)
        
class LastResult(Page):
    def is_displayed(self):
        return self.subsession.round_number == Constants.num_rounds 

    def vars_for_template(self):
        for p in self.subsession.get_players():
            
            #just for the bots to play just one repetition of APD and save cumulative payoffs
            p.cumulative_payoff=p.participant.vars['cumulative_payoff2']



page_sequence = [
        ShuffleWaitPage,
        Phase,
        Decision,
        ResultsWaitPage,
        LastResult,
    ]
