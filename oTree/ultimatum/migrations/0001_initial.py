# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from decimal import Decimal
import otree_save_the_change.mixins
import otree.db.models
from otree.common import Currency



class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('strategy', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('amount_offered', otree.db.models.CurrencyField(null=True, max_digits=12, choices=[(Currency(0), Currency(0)), (Currency(10), Currency(10)), (Currency(20), Currency(20)), (Currency(30), Currency(30)), (Currency(40), Currency(40)), (Currency(50), Currency(50)), (Currency(60), Currency(60)), (Currency(70), Currency(70)), (Currency(80), Currency(80)), (Currency(90), Currency(90)), (Currency(100), Currency(100))])),
                ('offer_accepted', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_0', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_10', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_20', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_30', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_40', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_50', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_60', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_70', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_80', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_90', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('response_100', otree.db.models.BooleanField(choices=[(True, 'Yes'), (False, 'No')])),
                ('session', otree.db.models.ForeignKey(related_name='ultimatum_group', to='otree.Session')),
            ],
            options={
                'db_table': 'ultimatum_group',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_index_in_game_pages', otree.db.models.PositiveIntegerField(default=0, null=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_group', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('group', otree.db.models.ForeignKey(to='ultimatum.Group', null=True)),
                ('participant', otree.db.models.ForeignKey(related_name='ultimatum_player', to='otree.Participant')),
                ('session', otree.db.models.ForeignKey(related_name='ultimatum_player', to='otree.Session')),
            ],
            options={
                'db_table': 'ultimatum_player',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Subsession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('session', otree.db.models.ForeignKey(related_name='ultimatum_subsession', to='otree.Session', null=True)),
            ],
            options={
                'db_table': 'ultimatum_subsession',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.AddField(
            model_name='player',
            name='subsession',
            field=otree.db.models.ForeignKey(to='ultimatum.Subsession'),
        ),
        migrations.AddField(
            model_name='group',
            name='subsession',
            field=otree.db.models.ForeignKey(to='ultimatum.Subsession'),
        ),
    ]
