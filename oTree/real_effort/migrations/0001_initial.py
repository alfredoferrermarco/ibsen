# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import otree.db.models
import django.core.validators
import otree_save_the_change.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('session', otree.db.models.ForeignKey(related_name='real_effort_group', to='otree.Session')),
            ],
            options={
                'db_table': 'real_effort_group',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_index_in_game_pages', otree.db.models.PositiveIntegerField(default=0, null=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_group', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('transcription_1', otree.db.models.TextField(null=True, validators=[django.core.validators.MaxLengthValidator(425)])),
                ('transcription_2', otree.db.models.TextField(null=True, validators=[django.core.validators.MaxLengthValidator(425)])),
                ('distance_1', otree.db.models.PositiveIntegerField(null=True)),
                ('distance_2', otree.db.models.PositiveIntegerField(null=True)),
                ('group', otree.db.models.ForeignKey(to='real_effort.Group', null=True)),
                ('participant', otree.db.models.ForeignKey(related_name='real_effort_player', to='otree.Participant')),
                ('session', otree.db.models.ForeignKey(related_name='real_effort_player', to='otree.Session')),
            ],
            options={
                'db_table': 'real_effort_player',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Subsession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('session', otree.db.models.ForeignKey(related_name='real_effort_subsession', to='otree.Session', null=True)),
            ],
            options={
                'db_table': 'real_effort_subsession',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.AddField(
            model_name='player',
            name='subsession',
            field=otree.db.models.ForeignKey(to='real_effort.Subsession'),
        ),
        migrations.AddField(
            model_name='group',
            name='subsession',
            field=otree.db.models.ForeignKey(to='real_effort.Subsession'),
        ),
    ]
