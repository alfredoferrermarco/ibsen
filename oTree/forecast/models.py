# -*- coding: utf-8 -*-
# <standard imports>
from __future__ import division

import random

import otree.models
from otree.db import models
from otree import widgets
from otree.common import Currency as c, currency_range, safe_json
from otree.constants import BaseConstants
from otree.models import BaseSubsession, BaseGroup, BasePlayer, BaseLink
# </standard imports>
import math

author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
	name_in_url = 'forecast'
	players_per_group = 6
	num_rounds = 18
	pf = 66
	var1 = 1300.0
	var2 = 49
	R = 1.05
	epsilon = 0

	training_1_choices = [
		'0',
		'1200',
		'1299',
		'1350'
	]

	training_1_correct = training_1_choices[2]

	training_2_choices = [
		'Period 10',
		'Period 20',
	]

	training_2_correct = training_2_choices[0]

class Subsession(BaseSubsession):
	updated_at = models.DateTimeField(auto_now_add=True)


class Group(BaseGroup):
	# <built-in>
	subsession = models.ForeignKey(Subsession)
	# </built-in>
	real_price = models.FloatField()
	updated_at = models.DateTimeField(auto_now_add=True)
	
	
	# method to set cash and shares to balance in previous round
	def set_real_price(self):
		pt_sum = 0
		for p in self.get_players():
			pt_sum += p.pe

		pt_avg = pt_sum/len(self.get_players())

		self.real_price = Constants.pf + 1.0/Constants.R * (pt_avg - Constants.pf) + Constants.epsilon    
	

class Link(BaseLink):
    pass

class Player(BasePlayer):
	# <built-in>
	subsession = models.ForeignKey(Subsession)
	group = models.ForeignKey(Group, null = True)
	# </built-in>

	training_question_1 = models.CharField(
		choices=Constants.training_1_choices,
		widget=widgets.RadioSelect(),
		#timeout_default=Constants.training_1_choices[1]
	)

	def is_training_question_1_correct(self):
		return self.training_question_1 == Constants.training_1_correct


	training_question_2 = models.CharField(
		choices=Constants.training_2_choices,
		widget=widgets.RadioSelect(),
		#timeout_default=Constants.training_1_choices[1]
	)

	def is_training_question_2_correct(self):
		return self.training_question_2 == Constants.training_2_correct

	# initial shares and payoff
	pe = models.PositiveIntegerField()

	cash = models.CurrencyField()
	period_points = models.FloatField()
	total_points = models.FloatField(default=0.0)
	
	def set_payoff(self):
		self.period_points = 0
		gr = self.group.in_round(self.round_number)
		if gr.real_price is not None:
			self.period_points = max(0, (Constants.var1 - Constants.var1/Constants.var2 * math.pow(self.in_round(self.round_number).pe - self.group.in_round(self.round_number).real_price,2)))
			if self.round_number > 1:
				self.total_points += self.in_previous_rounds()[-1].total_points + self.period_points
			else:
				self.total_points = self.period_points

			self.cash = self.total_points / 2600
			self.payoff = self.period_points
