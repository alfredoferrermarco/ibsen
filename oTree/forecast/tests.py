# -*- coding: utf-8 -*-
from __future__ import division

import random

from otree.common import Currency as c, currency_range

from . import views
from ._builtin import Bot
from .models import Constants


class PlayerBot(Bot):
    """Bot that plays one round"""

    def play_round(self):
        if self.subsession.round_number == 1:
            yield (views.Instruction)
            

            yield (
                views.Question1,
                {'training_question_1': '0'}
            )

            yield (
                views.Feedback1            
            )


            yield (
                views.Question2,
                {'training_question_2': 'Period 10'}
            )

            yield (
                views.Feedback2
            )

        
        yield (
            views.Results,
            {"pe": random.randint(30,50)}
        )

        

    def validate_play(self):
        pass
