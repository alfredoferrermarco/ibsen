# -*- coding: utf-8 -*-
# <standard imports>
from __future__ import division

import random

import otree.models
from otree.db import models
from otree import widgets
from otree.common import Currency as c, currency_range, safe_json
from otree.constants import BaseConstants
from otree.models import BaseSubsession, BaseGroup, BasePlayer, BaseLink
from django.utils import timezone
# </standard imports>

author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'neighbor_choice'
    players_per_group = 4
    num_rounds = 3

    #  Points made if player defects and the other cooperates""",
    defect_cooperate_amount = c(300)

    # Points made if both players cooperate
    cooperate_amount = c(200)
    cooperate_defect_amount = c(0)
    defect_amount = c(100)
    base_points = c(50)

    training_1_choices = [
        'Alice gets 300 points, Bob gets 0 points',
        'Alice gets 200 points, Bob gets 200 points',
        'Alice gets 0 points, Bob gets 300 points',
        'Alice gets 100 points, Bob gets 100 points'
    ]


    training_1_correct = training_1_choices[0]

class Subsession(BaseSubsession):

    pass


class Group(BaseGroup):
    pass

class Link(BaseLink):
    pass
    
class Player(BasePlayer):

    training_question_1 = models.CharField(
        choices=Constants.training_1_choices,
        widget=widgets.RadioSelect(),
        #timeout_default=Constants.training_1_choices[1]
    )

    def is_training_question_1_correct(self):
        return self.training_question_1 == Constants.training_1_correct

    decision = models.CharField(
        choices=['Cooperate', 'Defect'],
        doc="""This player's decision""",
        widget=widgets.RadioSelect()
    )

    def other_player(self):
        return self.get_others_in_group()[0]

    def set_payoff(self):
        points_matrix = {'Cooperate': {'Cooperate': Constants.cooperate_amount,
                                       'Defect': Constants.cooperate_defect_amount},
                         'Defect':   {'Cooperate': Constants.defect_cooperate_amount,
                                      'Defect': Constants.defect_amount}}
        neighbors = self.get_neighbors()
        self.payoff = 0
        for neighbor in neighbors:

            self.payoff += (points_matrix[self.decision]
                                           [neighbor.neighbor.decision])
        #self.payoff = (points_matrix[self.decision][self.other_player().decision])


class Request(models.Model):
	requester = models.ForeignKey(Player, related_name='requester')
	requested = models.ForeignKey(Player, related_name='requested')
	start_date = models.DateTimeField(auto_now_add=True)
	status = models.IntegerField(default=0)	