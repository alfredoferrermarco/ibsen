# -*- coding: utf-8 -*-
from __future__ import division

from otree.common import Currency as c, currency_range, safe_json

from . import models
from ._builtin import Page, WaitPage
from .models import Constants


def vars_for_all_templates(self):

    return {
             'total_q': 1,
        }


class Introduction(Page):

    timeout_seconds = 100



class Decision(Page):

    form_model = models.Player
    form_fields = ['decision']



class ResultsWaitPage(WaitPage):



    body_text = 'Waiting for the other participant to choose.'

    def after_all_players_arrive(self):
        if self.subsession.round_number == 1:
            self.subsession.set_neighbors([[2],[1,3],[4],[3]])
        if self.subsession.round_number != 1:
            self.subsession.neighbor_like_round(self.subsession.round_number -1)

        for p in self.group.get_players():
            p.set_payoff()


class Results(Page):

    def vars_for_template(self):


        self.player.set_payoff()
        decision_list = {}
        for neighbor in self.player.get_neighbors():
            decision_list[neighbor.neighbor.id_in_group] = neighbor.neighbor.decision.lower()

        return {
            'my_decision': self.player.decision.lower(),
            'other_player_decision': self.player.other_player().decision.lower(),
            'neighbors_decision': decision_list,
            'same_choice': self.player.decision == self.player.other_player().decision,
            'total_plus_base': self.player.payoff + Constants.base_points
        }

class Requests(Page):

    def vars_for_template(self):
        players = self.player.get_others_in_group()
        neighbors = self.player.get_neighbors()
        for neighbor in neighbors:
            players.remove(neighbor.neighbor)
        
        form_model = models.Request
        form_fields = ['requester']

        return {
            'players' : players
        }

    def before_next_page(self):
        post_dict = self.request.POST
        reqs = self.request.POST.getlist('player')
        for req in reqs:
            requested = models.Player.objects.filter(pk=req).first()
            request = models.Request(requester=self.player, requested=requested, status=0)
            request.save()

class NewNeighbors(Page):

    def vars_for_template(self):
        neighbors = [neighbor.neighbor for neighbor in self.player.get_neighbors()]
        return {
            'players' : neighbors
        }

class RequestsWaitPage(WaitPage):

    body_text = 'Waiting for the other participant to make requests.'

    def after_all_players_arrive(self):
        neighbor_list = []
        for player in self.group.get_players():
            requests = models.Request.objects.filter(requester=player).all()
            initial_neighbors = [ neighbor.neighbor for neighbor in player.get_neighbors()]

            for req in requests:
                initial_neighbors.append(req.requested)
            neighbor_list.append(initial_neighbors)
        print(neighbor_list)    
        self.subsession.set_neighbors(neighbor_list)




page_sequence = [
        Introduction,
        Decision,
        ResultsWaitPage,
        Results,
        Requests,
        RequestsWaitPage,
        NewNeighbors
        
    ]

