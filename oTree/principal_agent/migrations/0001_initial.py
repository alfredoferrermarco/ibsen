# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import otree.db.models
import otree_save_the_change.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('total_return', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('agent_fixed_pay', otree.db.models.CurrencyField(null=True, verbose_name=b'Fixed Payment (from -30 to 30)', max_digits=12)),
                ('agent_return_share', otree.db.models.FloatField(null=True, verbose_name=b'Return Share', choices=[[0.1, b'10%'], [0.2, b'20%'], [0.3, b'30%'], [0.4, b'40%'], [0.5, b'50%'], [0.6, b'60%'], [0.7, b'70%'], [0.8, b'80%'], [0.9, b'90%'], [1.0, b'100%']])),
                ('agent_work_effort', otree.db.models.PositiveIntegerField(null=True, choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)])),
                ('agent_work_cost', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('contract_accepted', otree.db.models.BooleanField(choices=[(True, b'Accept'), (False, b'Reject')])),
                ('session', otree.db.models.ForeignKey(related_name='principal_agent_group', to='otree.Session')),
            ],
            options={
                'db_table': 'principal_agent_group',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_index_in_game_pages', otree.db.models.PositiveIntegerField(default=0, null=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_group', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('training_my_payoff', otree.db.models.CurrencyField(null=True, verbose_name=b'I would receive', max_digits=12)),
                ('training_other_payoff', otree.db.models.CurrencyField(null=True, verbose_name=b'Participant B would receive', max_digits=12)),
                ('group', otree.db.models.ForeignKey(to='principal_agent.Group', null=True)),
                ('participant', otree.db.models.ForeignKey(related_name='principal_agent_player', to='otree.Participant')),
                ('session', otree.db.models.ForeignKey(related_name='principal_agent_player', to='otree.Session')),
            ],
            options={
                'db_table': 'principal_agent_player',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Subsession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('session', otree.db.models.ForeignKey(related_name='principal_agent_subsession', to='otree.Session', null=True)),
            ],
            options={
                'db_table': 'principal_agent_subsession',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.AddField(
            model_name='player',
            name='subsession',
            field=otree.db.models.ForeignKey(to='principal_agent.Subsession'),
        ),
        migrations.AddField(
            model_name='group',
            name='subsession',
            field=otree.db.models.ForeignKey(to='principal_agent.Subsession'),
        ),
    ]
