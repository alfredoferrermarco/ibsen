# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import otree.db.models
import otree_save_the_change.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('buyer_choice', otree.db.models.PositiveIntegerField(null=True)),
                ('session', otree.db.models.ForeignKey(related_name='lemon_market_group', to='otree.Session')),
            ],
            options={
                'db_table': 'lemon_market_group',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_index_in_game_pages', otree.db.models.PositiveIntegerField(default=0, null=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_group', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('payoff', otree.db.models.CurrencyField(null=True, max_digits=12)),
                ('training_buyer_earnings', otree.db.models.CurrencyField(null=True, verbose_name=b"Buyer's period payoff would be", max_digits=12)),
                ('training_seller1_earnings', otree.db.models.CurrencyField(null=True, verbose_name=b"Seller 1's period payoff would be", max_digits=12)),
                ('training_seller2_earnings', otree.db.models.CurrencyField(null=True, verbose_name=b"Seller 2's period payoff would be", max_digits=12)),
                ('price', otree.db.models.CurrencyField(null=True, verbose_name=b'Please indicate a price (from 0 to 50) you want to sell', max_digits=12)),
                ('quality', otree.db.models.CurrencyField(null=True, verbose_name=b'Please select a quality grade you want to produce', max_digits=12, choices=[(30, b'High'), (20, b'Medium'), (10, b'Low')])),
                ('choice', otree.db.models.PositiveIntegerField(blank=True, null=True, verbose_name=b'And you will', choices=[(1, b'Buy from seller 1'), (2, b'Buy from seller 2'), (0, b'Buy nothing')])),
                ('group', otree.db.models.ForeignKey(to='lemon_market.Group', null=True)),
                ('participant', otree.db.models.ForeignKey(related_name='lemon_market_player', to='otree.Participant')),
                ('session', otree.db.models.ForeignKey(related_name='lemon_market_player', to='otree.Session')),
            ],
            options={
                'db_table': 'lemon_market_player',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.CreateModel(
            name='Subsession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('final', otree.db.models.BooleanField(default=False, choices=[(True, 'Yes'), (False, 'No')])),
                ('session', otree.db.models.ForeignKey(related_name='lemon_market_subsession', to='otree.Session', null=True)),
            ],
            options={
                'db_table': 'lemon_market_subsession',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
        migrations.AddField(
            model_name='player',
            name='subsession',
            field=otree.db.models.ForeignKey(to='lemon_market.Subsession'),
        ),
        migrations.AddField(
            model_name='group',
            name='subsession',
            field=otree.db.models.ForeignKey(to='lemon_market.Subsession'),
        ),
    ]
