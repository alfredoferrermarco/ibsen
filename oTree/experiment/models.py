# -*- coding: utf-8 -*-
# <standard imports>
from __future__ import division

import random

import otree.models
from otree.db import models
from otree import widgets
from otree.common import Currency as c, currency_range, safe_json
from otree.constants import BaseConstants
from otree.models import BaseSubsession, BaseGroup, BasePlayer, BaseLink
# </standard imports>

author = 'M. Pereda'

doc = """
Hierachy Experiment II
"""


class Constants(BaseConstants):
    name_in_url = 'Experiment'
    players_per_group = None
    num_rounds = 1

    # define more constants here


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    # <built-in>
    subsession = models.ForeignKey(Subsession)
    # </built-in>

class Link(BaseLink):
    pass

class Player(BasePlayer):
    # <built-in>
    subsession = models.ForeignKey(Subsession)
    group = models.ForeignKey(Group, null=True)
    # </built-in>

    def role(self):
        # you can make this depend of self.id_in_group
        return ''

    cumulative_payoff = models.CurrencyField()   
    profile = models.CharField()
    multiplier = models.IntegerField()
    ind_rank_in_group = models.IntegerField()
    group_rank = models.IntegerField()
    automatic_decision = models.BooleanField()

    contribution = models.CurrencyField()
    questionsI = models.CurrencyField()
    questionsII = models.CharField()

    decision = models.CharField()


