# -*- coding: utf-8 -*-
from __future__ import division

## To show rate with two decimals and without the currency symbol
from re import sub
from decimal import Decimal

from otree.common import Currency as c, currency_range, safe_json

from . import models
from ._builtin import Page, WaitPage
from .models import Constants

def vars_for_all_templates(self):
    return {
            'rate':  Decimal(sub(r'[^\d.]', '', str(1 / c(1).to_real_world_currency(self.session)))),
            'rate_integer':int(1 / c(1).to_real_world_currency(self.session)),
    }


class MyPage(Page):
    pass

class ResultsWaitPage(WaitPage):

    def after_all_players_arrive(self):
        pass

class TrialQuestions(Page):
    pass

class Portada(Page):
    pass


page_sequence = [
	Portada,
    MyPage,
]
