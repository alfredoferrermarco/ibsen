# urls.py
from django.conf.urls import url
from otree.default_urls import urlpatterns

urlpatterns.append(url(r'^send_message$', 'pgg_chat.custom_views.send_message'))
urlpatterns.append(url(r'^get_messages$', 'pgg_chat.custom_views.get_messages'))