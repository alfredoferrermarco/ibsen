from otree.api import Currency as c, currency_range
from . import models
from ._builtin import Page, WaitPage
from .models import Constants
from . import models
from django.http import HttpResponse


class Introduction(Page):

	def is_displayed(self):
		return self.round_number == 1
    


class Contribute(Page):

	form_model = models.Player
	form_fields = ['contribution']

class ResultsWaitPage(WaitPage):

    def after_all_players_arrive(self):
        self.group.set_payoffs()


class Results(Page):
    pass





page_sequence = [
    Introduction,
    Contribute,
    ResultsWaitPage,
    Results
]


