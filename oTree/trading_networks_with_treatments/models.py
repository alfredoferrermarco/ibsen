from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer, BaseLink,
    Currency as c, currency_range
)

import random
import igraph 
import logging
import json
import copy
import datetime
from django.utils import timezone
from django.core.cache import cache
from django.utils.translation import ugettext_lazy as _
logger = logging.getLogger('django')


author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'trading_networks_with_treatments'
    players_per_group = 50
    num_rounds = 60
    max_cost = 100
    colors = {'source': '#f00', 'destination': '#f00', 'intermediary':'#ccc', 'player': '#0f0', 'link_shortest_color':'#f00', 'link_player_color': '#00f'}
    # s y d no pueden ser contiguos
    # 100 payoffs
    # enteros entre 0 y 100 pueden elegir
    


class Subsession(BaseSubsession):
    created_at = models.DateTimeField(auto_now_add=True)
    
    def before_session_starts(self):
        if self.round_number % 15 == 1:
            matrix = self.get_group_matrix()
            new_matrix = []
            for row in matrix:
                random.shuffle(row)
            self.set_group_matrix(matrix)
            
        else:
            self.group_like_round(self.round_number -1 )

        for group in self.get_groups():
            edges = []
            nodes = []
            if self.session.config['treatment'] == 'random':    
                csv_file=open('trading_networks_with_treatments/static/random.json', 'r')
                diameter = 5
            elif self.session.config['treatment'] == 'regular':
                csv_file=open('trading_networks_with_treatments/static/regular.json', 'r')
                diameter = 11
            else:
                csv_file=open('trading_networks_with_treatments/static/sw.json', 'r')
                diameter = 7
            data = json.load(csv_file)
            list_links = [[] for i in range(Constants.players_per_group)]
            g = igraph.Graph()
            g.add_vertices(Constants.players_per_group)
            for link in data['edges']:
                list_links[int(link['source'])].append(int(link['target']) + 1)
                edges.append({"id": link['id'], "source": str(int(link['source']) + 1), "target": str(int(link['target']) + 1), "color": "#ccc", "size":1 })
                g.add_edge(int(link['source']),int(link['target']))
            
            
            if self.round_number % 15 == 1:
                players = group.get_players() 
                for p in players:
                    if p.participant.id_in_session % 50 == 0:
                        group.source = p.id_in_group
                    if p.participant.id_in_session % 50 == 49:
                        group.destination = p.id_in_group
                distance = len(g.get_shortest_paths(group.source-1,to=group.destination-1)[0])
                logger.info("DISTANCIA grupo {0} {1}".format(group.id_in_subsession, distance))   
                while distance <= diameter:
                    random.shuffle(players)
                    group.set_players(players)
                    for p in players:
                        if p.participant.id_in_session % 50 == 0:
                            group.source = p.id_in_group
                        if p.participant.id_in_session % 50 == 49:
                            group.destination = p.id_in_group
                    distance = len(g.get_shortest_paths(group.source-1,to=group.destination-1)[0])
                    logger.info("DISTANCIA grupo {0} {1}".format(group.id_in_subsession, distance))   
                group.set_neighbors(list_links)
            else:
                previous_group = group.in_round(self.round_number -1)
                group.source = previous_group.source
                group.destination = previous_group.destination
                group.set_neighbors(list_links)

            for node in data['nodes']:
                p = group.get_player_by_id(int(node['id']) + 1)
                node_attr = {"id": str(p.id_in_group), "color": Constants.colors[p.role()], "size":15, "label": "", "x": node['x'], "y": node['y'] }
                if p.id_in_group == group.source:
                    node_attr['label'] += str(_("source"))
                if p.id_in_group == group.destination:
                    node_attr['label'] += str(_("destination"))

                nodes.append(node_attr)

            cache.set(str(self.session.id) + "r" + str(self.round_number) + "g" +  str(group.id_in_subsession) + "edges", edges, 86400)    
            cache.set(str(self.session.id) + "r" + str(self.round_number) + "g" +  str(group.id_in_subsession) + "nodes", nodes, 86400)    

        
            
            

class Group(BaseGroup):
    source = models.PositiveIntegerField(initial=0)
    destination = models.PositiveIntegerField(initial=0)
    deal = models.BooleanField(initial=False)
    path = models.CharField(max_length=255)
    total_cost = models.CurrencyField(min=0, max=100, initial=0)
    updated_at = models.DateTimeField(auto_now_add=True)


    def set_payoffs(self):
        t1 = datetime.datetime.now()    
        g = igraph.Graph()
        nodes = cache.get(str(self.session.id) + "r" + str(self.round_number) + "g" +  str(self.id_in_subsession) + "nodes")    
        for node in nodes:
            p = self.get_player_by_id(int(node['id']))
            g.add_vertex(name=node['id'], label=node['id'], overrun=p.overrun)
            if p.role() == "intermediary":
                node['label'] += str(p.overrun)
        cache.set(str(self.session.id) + "r" + str(self.round_number) + "g" +  str(self.id_in_subsession) + "nodes", nodes, 86400)    

        
        for p in self.get_players():
            for neighbor in p.get_neighbors():
                g.add_edge(g.vs.find(str(p.id_in_group)),g.vs.find(str(neighbor.neighbor.id_in_group)), weight=(p.overrun + neighbor.neighbor.overrun) / 2.0)
        #logger.info(str(self.source))
        #logger.info(str(self.destination))
        #logger.info(g.vs.find(str(self.source)))
        #logger.info(g.vs.find(str(self.destination)))
        v_path = random.choice(g.get_all_shortest_paths(g.vs.find(str(self.source)),to=g.vs.find(str(self.destination)), weights=g.es["weight"], mode=igraph.ALL))
        self.total_cost = 0
        s_path = []
        for index in v_path:
            s_path.append(int(g.vs[index]['label']))
            self.total_cost += g.vs[index]["overrun"]
        #logger.info("{0} {1}".format( v_path[0], self.total_cost))
        v_path = [x+1 for x in v_path]
        if self.total_cost <= Constants.max_cost:
            self.deal = True
            self.path = s_path
            payoff_s_and_d = float((100-self.total_cost)) / 2.0
        else:
            payoff_s_and_d = 0

        for p in self.get_players():
            if p.role() == "intermediary":
                path_from_s_to_p = random.choice(g.get_all_shortest_paths(g.vs.select(label_eq=str(self.source))[0],to=g.vs.select(label_eq=str(p.id_in_group))[0], weights=g.es["weight"], mode=igraph.ALL))[:-1]
                total_path = []
                for v in path_from_s_to_p:
                    total_path.append(int(g.vs[v]["name"]))
                path_from_p_to_d = random.choice(g.get_all_shortest_paths(g.vs.select(label_eq=str(p.id_in_group))[0],to=g.vs.select(label_eq=str(self.destination))[0], weights=g.es["weight"], mode=igraph.ALL))
                for v in path_from_p_to_d:
                    total_path.append(int(g.vs[v]["name"]))
                #logger.info(total_path)
                for index in total_path:
                    p.total_cost += g.vs.find(str(index))["overrun"]
                #logger.info("{0} {1}".format(p.total_cost,p.id_in_group))
                p.path = total_path
                if self.path:
                    list_path = list(self.path)
                    if p.id_in_group in list_path:
                        p.payoff = p.overrun
            else:
                p.path = s_path
                p.total_cost = self.total_cost
                p.payoff = 0#payoff_s_and_d
            
        self.updated_at = timezone.now()    
        logger.info("Tiempo de calcular los caminos de todos {0} para el grupo {1}".format(datetime.datetime.now() - t1,self.id_in_subsession))

                
class Player(BasePlayer):
    overrun = models.PositiveIntegerField(min=0, max=100, initial=None, verbose_name=_("your_price"))
    question_neighbors_connected = models.PositiveIntegerField(default=0)
    question_centrality = models.PositiveIntegerField(default=0)
    path = models.CharField(max_length=255)
    total_cost = models.CurrencyField(min=0, max=100, default=0, initial=0)

    
    def role(self):
        if self.id_in_group==self.group.source:
            return "source"
        elif self.id_in_group==self.group.destination:
            return "destination"
        else:
            return "intermediary"


class Link(BaseLink):
    pass