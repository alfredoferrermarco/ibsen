# -*- coding: utf-8 -*-
# <standard imports>
from __future__ import division

import random

import otree.models
from otree.db import models
from otree import widgets
from otree.common import Currency as c, currency_range, safe_json
from otree.constants import BaseConstants
from otree.models import BaseSubsession, BaseGroup, BasePlayer, BaseLink
# </standard imports>

author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'my_payments'
    players_per_group = None
    num_rounds = 1
    Show_up_fee = 5


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass
class Link(BaseLink):
    pass


class Player(BasePlayer):
    earnings = models.CurrencyField()
    Total_earnings = models.CurrencyField()
    earnings_rounded = models.IntegerField()
    Total_earnings_rounded = models.IntegerField()
    cumulative_payoff = models.CurrencyField()