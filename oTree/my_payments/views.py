# -*- coding: utf-8 -*-
from __future__ import division

from otree.common import Currency as c, currency_range, safe_json

from . import models
from ._builtin import Page, WaitPage
from .models import Constants
from decimal import Decimal

from re import sub


def vars_for_all_templates(self):

    return {
        'show_up': Constants.Show_up_fee,
        'rate_integer':int(1 / c(1).to_real_world_currency(self.session)),
        }


class ResultsWaitPage(WaitPage):
	wait_for_all_groups = True

	def after_all_players_arrive(self):
		pass

class FinalResult(Page):

    def vars_for_template(self):

        for p in self.subsession.get_players():

            if self.session.config['repetitions'] > 1:
                p.participant.vars['cumulative_payoff'] = p.participant.vars['cumulative_payoff1'] + p.participant.vars['cumulative_payoff2']
            else:
                p.participant.vars['cumulative_payoff'] = p.participant.vars['cumulative_payoff1']
            
            p.cumulative_payoff = p.participant.vars['cumulative_payoff'] 
            p.participant.vars['payoff'] = p.cumulative_payoff 
            p.participant.vars['earnings']=c(p.participant.vars['cumulative_payoff']).to_real_world_currency(self.session)
            total_points = (p.participant.vars['cumulative_payoff'] + c(Constants.Show_up_fee)/(c(1).to_real_world_currency(self.session)))
            p.participant.vars['Total_earnings'] = total_points.to_real_world_currency(self.session)
            p.participant.vars['earnings_rounded'] = round(Decimal(sub(r'[^\d.]', '', str( p.participant.vars['earnings'] ))),0)
            p.participant.vars['Total_earnings_rounded'] = round(Decimal(sub(r'[^\d.]', '', str( p.participant.vars['Total_earnings'] ))),0)


            p.earnings=p.participant.vars['earnings']
            p.Total_earnings = p.participant.vars['Total_earnings']
            p.earnings_rounded = p.participant.vars['earnings_rounded']
            p.Total_earnings_rounded = p.participant.vars['Total_earnings_rounded']


page_sequence = [
    FinalResult,
]
