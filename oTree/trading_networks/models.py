from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer, BaseLink,
    Currency as c, currency_range
)

import random
import igraph 


author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'trading_networks'
    players_per_group = 12
    num_rounds = 10
    colors = {'source': '#0f0', 'destination': '#0f0', 'intermediary':'#00f'}
    treatment_change_round = 5
    # s y d no pueden ser contiguos
    # 100 payoffs
    # enteros entre 0 y 100 pueden elegir
    


class Subsession(BaseSubsession):
    created_at = models.DateTimeField(auto_now_add=True)
    
    def before_session_starts(self):
        if self.round_number == 1 or self.round_number == Constants.treatment_change_round:
            matrix = self.get_group_matrix()
            new_matrix = []
            for row in matrix:
                random.shuffle(row)
            self.set_group_matrix(matrix)

        for group in self.get_groups():
            if self.round_number >= Constants.treatment_change_round and group.id_in_subsession % 2 == 0:    
                group.source = 0
                group.destination = Constants.players_per_group + 1
                group.set_neighbors([[2],[1,3],[2],[5],[4,6],[5],[8],[7,9],[8],[11],[10,12],[11]])
            else:
                group.set_neighbors([[2,3,5,6,8,9,11,12],[1,3,4],[1,2,4],[2,3,5],[1,4,6,10],[1,5,7],[6,8],[1,7,9],[1,8,10],[5,9,11,12],[1,10,12],[1,10,11]])
                group.source = random.randint(1,Constants.players_per_group)
                source_player = group.get_player_by_id(group.source)
                neighbor_list = [n.neighbor.id_in_group for n in source_player.get_neighbors()]
                neighbor_list.append(source_player.id_in_group)
                group.destination  = random.randint(1,Constants.players_per_group)
                while group.destination in neighbor_list:
                    group.destination  = random.randint(1, Constants.players_per_group)
            
                
                

            



class Group(BaseGroup):
    source = models.PositiveIntegerField(initial=0)
    destination = models.PositiveIntegerField(initial=0)
    deal = models.BooleanField(initial=False)
    path = models.CharField(max_length=255)
    total_cost = models.CurrencyField(min=0, max=100, initial=None, verbose_name='¿Cuál será tu sobrecoste?')


    def set_payoffs(self):
        g = igraph.Graph()

        if self.subsession.round_number >= Constants.treatment_change_round and self.id_in_subsession % 2 == 0:
            g.add_vertices(Constants.players_per_group + 2)
            g.vs[0]["overrun"] = 0
            g.vs[Constants.players_per_group + 1]["overrun"] = 0
            for p in self.get_players():
                g.vs[p.id_in_group]["overrun"] = p.overrun
                # adding artificial links to fake nodes
                if p.id_in_group in (1,4,7,10):
                    g.add_edge(0,p.id_in_group, weight=p.overrun/2.0)
                elif p.id_in_group in (3,6,9,12):
                    g.add_edge(13,p.id_in_group, weight=p.overrun/2.0)
                for neighbor in p.get_neighbors():
                    g.add_edge(p.id_in_group,neighbor.neighbor.id_in_group, weight=(p.overrun + neighbor.neighbor.overrun) / 2.0)

            
            v_path = g.get_shortest_paths(self.source,to=self.destination, weights=g.es["weight"], mode=igraph.ALL)
            self.total_cost = 0
            for index in v_path[0]:
                self.total_cost += g.vs[index]["overrun"]
        else:
            g.add_vertices(Constants.players_per_group)
            for p in self.get_players():
                g.vs[p.id_in_group-1]["overrun"] = p.overrun
                for neighbor in p.get_neighbors():
                    g.add_edge(p.id_in_group-1,neighbor.neighbor.id_in_group-1, weight=(p.overrun + neighbor.neighbor.overrun) / 2.0)
            v_path = g.get_shortest_paths(self.source-1,to=self.destination-1, weights=g.es["weight"], mode=igraph.ALL)
            self.total_cost = 0
            for index in v_path[0]:
                self.total_cost += g.vs[index]["overrun"]
            v_path[0] = [x+1 for x in v_path[0]]


        

        if self.total_cost <= 100:
            self.deal = True
            self.path = v_path[0]
            payoff_s_and_d = float((100-self.total_cost)) / 2.0
            for p in self.get_players():
                if p.role() == "source" or p.role() == "destination":
                    p.payoff = payoff_s_and_d
                elif p.id_in_group in v_path[0]:
                    p.payoff = p.overrun
                else:
                    p.payoff = 0
                
class Player(BasePlayer):
    overrun = models.CurrencyField(min=0, max=100, initial=None, verbose_name='¿Cuál será tu sobrecoste?')
    
    def role(self):
        if self.id_in_group==self.group.source:
            return "source"
        elif self.id_in_group==self.group.destination:
            return "destination"
        else:
            return "intermediary"


class Link(BaseLink):
    pass