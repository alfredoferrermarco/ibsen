from otree.api import Currency as c, currency_range
from . import models
from ._builtin import Page, WaitPage
from .models import Constants
from django.utils import timezone

import datetime
import math
import random


class WaitStartPage(Page):
    def is_displayed(self):
        return self.subsession.round_number == 1 

    
class Instructions(Page):
    timeout_seconds = 15

    #def set_extra_attributes(self):
    #    self.timeout_seconds = (self.subsession.created_at + datetime.timedelta(seconds=20 * self.subsession.round_number) - timezone.now()).total_seconds()

    def is_displayed(self):
        return self.subsession.round_number == 1 


class InstructionsWaitPage(WaitPage):
    #body_main
    def is_displayed(self):
        return self.subsession.round_number == 1 
    def after_all_players_arrive(self):
        pass



class Decision(Page):
    form_model = models.Player
    form_fields = ['overrun']

    timeout_seconds = 20
    #timeout_submission = {'overrun': random.randint(1,99)}

    def vars_for_template(self):
        if self.subsession.round_number >= Constants.treatment_change_round:
            data = {}
            treatment = "no_network"
        else:
            treatment = "network"
            neighbor_matrix = self.group.get_neighbor_matrix()
            alfa = (2*math.pi) / 12.0

            nodes = [ {"id": str(player.id_in_group), "color": Constants.colors[player.role()], "size":40, "label": str(player.id_in_group), "x": 150 - math.cos((player.id_in_group -1) * alfa  - math.pi/2.0) * 150  , "y": 150 + math.sin((player.id_in_group -1) * alfa - math.pi/2.0) * 150} for player in self.group.get_players() ]
            nodes[self.player.id_in_group - 1]["label"] += " YOU"
            nodes[self.group.source -1]["label"] += " S"
            nodes[self.group.destination -1]["label"] += " D"
            edges = []
            i = 0
            for index,l in enumerate(neighbor_matrix):
                for n in l:
                    i += 1
                    edges.append({"id": str(i), "source": str(index+1), "target": str(n.neighbor.id_in_group), "color": "#ccc"})


            data = {"nodes": nodes, "edges": edges}
        return {
            'data': data,   
            'role': self.player.role(),
            'treatment' : treatment


        }


    def before_next_page(self):
        if self.timeout_happened:
            if self.player.role() == "intermediary":
                self.player.overrun = random.randint(0,33)
            else:
                self.player.overrun = 0

class DecisionWaitPage(WaitPage):
    
    def after_all_players_arrive(self):
        self.group.set_payoffs()
        


class Results(Page):
    timeout_seconds = 15
    def vars_for_template(self):
        fake_players = False
        fake_players_payoff = 0
        selected_path = ""
        if self.subsession.round_number >= Constants.treatment_change_round:
            treatment = "no_network"
            data = {}
            if self.group.path:
                for node in list(self.group.path[1:-1].replace(' ', '').split(',')):
                    selected_path += ", Player " + node
                selected_path = selected_path[1:]
            if self.group.id_in_subsession % 2 == 0:
                fake_players = True
                fake_players_payoff = float((100-self.group.total_cost)) / 2.0 if self.group.deal else 0

        else:
            treatment = "network"
            neighbor_matrix = self.group.get_neighbor_matrix()
            alfa = (2*math.pi) / 12.0

            nodes = [ {"id": str(player.id_in_group), "color": Constants.colors[player.role()], "size":40, "label": str(player.id_in_group), "x": 150 - math.cos((player.id_in_group -1) * alfa  - math.pi/2.0) * 150  , "y": 150 + math.sin((player.id_in_group -1) * alfa - math.pi/2.0) * 150} for player in self.group.get_players() ]
            
            nodes[self.player.id_in_group - 1]["label"] += " YOU"
            nodes[self.group.source -1]["label"] += " S"
            nodes[self.group.destination -1]["label"] += " D"

            edges = []
            i = 0
            for index,l in enumerate(neighbor_matrix):
                for n in l:
                    i += 1
                    edges.append({"id": str(i), "source": str(index+1), "target": str(n.neighbor.id_in_group), "color": "#ccc"})
            data = {"nodes": nodes, "edges": edges}

            if self.group.path:
                for node in list(self.group.path[1:-1].replace(' ', '').split(',')):
                    selected_path += ", Player " + str(int(node))
                selected_path = selected_path[1:]
        ####
        


        return {
            'data': data,    
            'payoff': self.player.payoff,
            'players': self.group.get_players(),
            'selected_path': selected_path,
            'treatment' : treatment,
            'fake_players': fake_players,
            'fake_players_payoff': fake_players_payoff
        
        }

class InstructionsSecondPhase(Page):
    timeout_seconds = 15
    def is_displayed(self):
        return self.subsession.round_number == Constants.treatment_change_round      

class InstructionsSecondPhaseWaitPage(WaitPage):
    def is_displayed(self):
        return self.subsession.round_number == Constants.treatment_change_round

    def after_all_players_arrive(self):
        pass

class ShowMoneyWaitPage(WaitPage):
    wait_for_all_groups = True

    def is_displayed(self):
        return self.subsession.round_number == Constants.num_rounds
    def after_all_players_arrive(self):
        if self.subsession.round_number == Constants.num_rounds:
            total_payoff = 0
            for p in self.session.get_participants():
                total_payoff += p.payoff
            self.session.config['real_world_currency_per_point'] = self.session.config['budget'] / total_payoff
            
class ShowMoney(Page):
    def is_displayed(self):
        return self.subsession.round_number == Constants.num_rounds



page_sequence = [
    #WaitStartPage,
    Instructions,
    InstructionsWaitPage,
    InstructionsSecondPhase,
    InstructionsSecondPhaseWaitPage,
    Decision,
    DecisionWaitPage,
    Results,
    ShowMoneyWaitPage,
    ShowMoney,
]
