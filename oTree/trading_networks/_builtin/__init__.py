# This file is auto-generated.
# It's used to aid autocompletion in code editors.

import otree.api
from .. import models


class Page(otree.api.Page):
    def z_autocomplete(self):
        self.subsession = models.Subsession()
        self.group = models.Group()
        self.player = models.Player()
        self.links = models.Links()


class WaitPage(otree.api.WaitPage):
    def z_autocomplete(self):
        self.subsession = models.Subsession()
        self.group = models.Group()
        self.links = models.Links()


class Bot(otree.api.Bot):
    def z_autocomplete(self):
        self.subsession = models.Subsession()
        self.group = models.Group()
        self.player = models.Player()
        self.links = models.Links()


