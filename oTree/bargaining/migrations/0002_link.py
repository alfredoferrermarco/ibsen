# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import otree.db.models
import otree_save_the_change.mixins


class Migration(migrations.Migration):

    dependencies = [
        ('otree', '__first__'),
        ('bargaining', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Link',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_is_missing_players', otree.db.models.BooleanField(default=False, db_index=True, choices=[(True, 'Yes'), (False, 'No')])),
                ('id_in_subsession', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('round_number', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('id_in_player', otree.db.models.PositiveIntegerField(null=True, db_index=True)),
                ('player', otree.db.models.ForeignKey(to='bargaining.Player', null=True)),
                ('session', otree.db.models.ForeignKey(related_name='bargaining_link', to='otree.Session')),
                ('subsession', otree.db.models.ForeignKey(to='bargaining.Subsession')),
            ],
            options={
                'db_table': 'bargaining_link',
            },
            bases=(otree_save_the_change.mixins.SaveTheChange, models.Model),
        ),
    ]
