# In consumers.py
from channels import Group
from channels.sessions import channel_session
import json
import datetime
from . import models
import logging
logger = logging.getLogger(__name__)

# Connected to websocket.connect
def ws_connect(message):
	prefix, label = message['path'].strip('/').split('/')
	Group('chat-' + label).add(message.reply_channel)

# Connected to websocket.disconnect
def ws_disconnect(message):
	prefix, label = message['path'].strip('/').split('/')
	Group('chat-' + label).discard(message.reply_channel)


def ws_receive(message):
	t1 = datetime.datetime.now()
	prefix, label = message['path'].strip('/').split('/')
	data = json.loads(message['text'])
	data["created_at"] = datetime.datetime.now().strftime('%H:%M:%S')
	Group('chat-' + label).send({'text': json.dumps(data)})
	player_id = data['player_id']
	group_id = data['group_id']
	message = data['message']
	round_number = data['round_number']
	#db_message = models.Message(player_id=player_id, group_id=group_id, data=message, round_number=round_number)
	#db_message.save()
	logger.debug("{0}-{1}-{2}-{3}".format(player_id,group_id,round_number,message))

	print ("TIempo en milisegundos", (datetime.datetime.now() - t1).total_seconds() * 1000)

	