from channels.routing import route
from chat_channels.consumers import ws_connect, ws_disconnect, ws_receive
from otree.channels.default_routing import channel_routing

channel_routing += [
    route("websocket.connect", ws_connect, path=r"^/chat"),
    route("websocket.disconnect", ws_disconnect, path=r"^/chat"),
    route("websocket.receive", ws_receive, path=r"^/chat"),
]