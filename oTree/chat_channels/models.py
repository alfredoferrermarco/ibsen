from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer, BaseLink,
    Currency as c, currency_range
)

from otree.db import models
author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
	name_in_url = 'chat_channels'
	players_per_group = 15
	num_rounds = 10
	multiplier = 2
	endowment = c(100)


class Subsession(BaseSubsession):
	pass


class Group(BaseGroup):
	total_contribution  = models.CurrencyField()
	individual_share = models.CurrencyField()

	def set_payoffs(self):
		self.total_contribution = sum([p.contribution for p in self.get_players()])
		self.individual_share = self.total_contribution * Constants.multiplier / Constants.players_per_group
		for p in self.get_players():
			p.payoff = (Constants.endowment - p.contribution) + self.individual_share


class Player(BasePlayer):
	contribution = models.CurrencyField(min=0, max=Constants.endowment, doc="""The amount contributed by the player""")

	def natural_key(self):
		return (self.id_in_group)
	

class Link(BaseLink):
	pass


class Message(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	player = models.ForeignKey(Player, null=True)
	group = models.ForeignKey(Group, null=True)
	round_number = models.IntegerField(null=True)
	data = models.CharField(max_length=150)
