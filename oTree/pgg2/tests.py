# -*- coding: utf-8 -*-
from __future__ import division

import random

from otree.common import Currency as c, currency_range

from . import views
from ._builtin import Bot
from .models import Constants


class PlayerBot(Bot):
    #"" two rounds ""


    def play_round(self):
    	# round 1 

    	if self.subsession.round_number == 1:
        	self.submit(views.FaseI)
        	

        #All rounds
        self.submit(views.Contribute, {'contribution': random.choice(range(0,Constants.endowment + 1,2))})

  
        #Final round
        if self.subsession.round_number == Constants.num_rounds:
        	self.submit(views.FinalResult)
        

    def validate_play(self):
        pass
